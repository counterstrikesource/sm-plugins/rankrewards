#pragma semicolon 1
#pragma newdecls required

public Plugin myinfo = 
{
	name = "Rank Rewards",
	author = "Dolly, maxime1907",
	description = "Display rank rewards",
	version = "1.0",
	url = ""
}

public void OnPluginStart()
{
    RegConsoleCmd("sm_rewards", Rewards);
}

public Action Rewards(int client, int args)
{    
    Menu menu = new Menu(Menu_Callback);
    menu.SetTitle("The rewards of top10 players every month will be like this following");
    menu.AddItem("0", "Rank#1 - 15 days of VIP");
    menu.AddItem("1", "Rank#2 - 14 days of VIP");
    menu.AddItem("2", "Rank#3 - 14 days of VIP");
    menu.AddItem("3", "Rank#4 - 12 days of VIP");
    menu.AddItem("4", "Rank#5 - 10 days of VIP");
    menu.AddItem("5", "Rank#6 - 10 days of VIP");
    menu.AddItem("6", "Rank#7 - 8 days of VIP");
    menu.AddItem("7", "Rank#8 - 8 days of VIP");
    menu.AddItem("8", "Rank#9 - 8 days of VIP");
    menu.AddItem("9", "Rank#10 - 8 days of VIP");
    menu.Display(client, 32);
    menu.ExitButton = true;
    return Plugin_Handled;
}

public int Menu_Callback (Menu menu, MenuAction action, int client, int option) // let's replace param1 with client and param2 with option so it's easier to understand
{
    if (action == MenuAction_Select)
    {
        switch(option)
        {
            case 0:
            {
                ///****//
            }
            case 1:
            {
                ///****//
            }
            case 2:
            {
                ///****//
            }
            case 3:
            {
                ///****//
            }
            case 4:
            {
                ///****//
            }
            case 5:
            {
                ///****//
            }
            case 6:
            {
                ///****//
            }
            case 7:
            {
                ///****//
            }
            case 8:
            {
                ///****//
            }
            case 9:
            {
                ///****//
                }
            }
        }
    }